FROM ubuntu:latest

MAINTAINER Jakub Jelen <jjelen@redhat.com>

# This is required because Ubuntu installation is interactive
# even if the -y is provided (sic)
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update
RUN apt-get install -y \
        build-essential \
        cmake \
        clang \
        clang-tools \
        doxygen \
        dropbear \
        gcc \
        git \
        lcov \
        libgcrypt20-dev \
        llvm \
        make \
        libasan5 \
        libcmocka-dev \
        libmbedtls-dev \
        libnss-wrapper \
        libpam-wrapper \
        libp11-dev \
        libresolv-wrapper \
        libsocket-wrapper \
        libssl-dev \
        libubsan1 \
        libuid-wrapper \
        libz-dev \
        netcat \
        openssh-client \
        openssh-server \
        softhsm \
        valgrind
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/*
# This directory is needed for openssh server to start. It is probably
# supposed to be created by tmpfiles, but they will not work in containers
RUN mkdir /run/sshd
